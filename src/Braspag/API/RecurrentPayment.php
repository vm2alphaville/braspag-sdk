<?php
namespace Braspag\API;

class RecurrentPayment implements \JsonSerializable
{
	
	const REASON_SUCCESSFUL = 0;
    const REASON_AFFILIATIONNOTFOUND = 1;
    const REASON_ISSUFICIENTFUNDS = 2;
    const REASON_COULDNOTGETCREDITCARD = 3;
    const REASON_CONNECTIONWITHACQUIRERFAILED = 4;
    const REASON_INVALIDTRANSACTIONTYPE = 5;
    const REASON_INVALIDPAYMENTPLAN = 6;
    const REASON_DENIED = 7;
    const REASON_SCHEDULED = 8;
    const REASON_WAITING = 9;
    const REASON_AUTHENTICATED = 10;
    const REASON_NOTAUTHENTICATED = 11;
    const REASON_PROBLEMWITHCREDITCARD = 12;
    const REASON_CARDCANCELED = 13;
    const REASON_BLOCKEDCREDITCARD = 14;
    const REASON_CARDEXPIRED = 15;
    const REASON_ABORTEDBYFRAUD = 16;
    const REASON_COULDNOTANTIFRAUD = 17;
    const REASON_TRYAGAIN = 18;
    const REASON_INVALIDAMOUNT = 19;
    const REASON_PROBLEMSWITHISSUER = 20;
    const REASON_INVALIDCARDNUMBER = 21;
    const REASON_TIMEOUT = 22;
    const REASON_CARTAOPROTEGIDOISNOTENABLED = 23;
    const REASON_PAYMNETMETHODNOTENABLED = 24;
    const REASON_INVALIDREQUEST = 98;
    const REASON_INTERNALERROR = 99;
    const REASON_LABEL = [
        self::REASON_SUCCESSFUL => 'Sucesso',
        self::REASON_AFFILIATIONNOTFOUND => 'Filiação não encontrada',
        self::REASON_ISSUFICIENTFUNDS => 'Fundos Emissores',
        self::REASON_COULDNOTGETCREDITCARD => 'Não poderia obter um cartão de crédito',
        self::REASON_CONNECTIONWITHACQUIRERFAILED => 'Conexão com o adiquiridor falhou',
        self::REASON_INVALIDTRANSACTIONTYPE => 'Tipo de transação inválida',
        self::REASON_INVALIDPAYMENTPLAN => 'Plano de pagamento inválido',
        self::REASON_DENIED => 'Negado',
        self::REASON_SCHEDULED => 'Programação',
        self::REASON_WAITING => 'Aguardando',
        self::REASON_AUTHENTICATED => 'Autenticado',
        self::REASON_NOTAUTHENTICATED => 'Não autenticado',
        self::REASON_PROBLEMWITHCREDITCARD => 'Problemas com o cartão de crédito',
        self::REASON_CARDCANCELED => 'Cartão cancelado',
        self::REASON_BLOCKEDCREDITCARD => 'Cartão de crédito bloqueado',
        self::REASON_CARDEXPIRED => 'Cartão expirado',
        self::REASON_ABORTEDBYFRAUD => 'Abortado por fraude',
        self::REASON_COULDNOTANTIFRAUD => 'Não pode anti-fraudar',
        self::REASON_TRYAGAIN => 'Tente novamente mais tarde',
        self::REASON_INVALIDAMOUNT => 'Montante inválido',
        self::REASON_PROBLEMSWITHISSUER => 'Problemas com o emissor',
        self::REASON_INVALIDCARDNUMBER => 'Número do cartão inválido',
        self::REASON_TIMEOUT => 'Tempo esgotado',
        self::REASON_CARTAOPROTEGIDOISNOTENABLED => 'Cartão protegido não está disponível',
        self::REASON_PAYMNETMETHODNOTENABLED => 'Método de pagamento não está disponível',
        self::REASON_INVALIDREQUEST => 'Pedido inválido',
        self::REASON_INTERNALERROR => 'Erro interno',
    ];

    const INTERVAL_MONTHLY = 'Monthly';

    const INTERVAL_BIMONTHLY = 'Bimonthly';

    const INTERVAL_QUARTERLY = 'Quarterly';

    const INTERVAL_SEMIANNUAL = 'SemiAnnual';

    const INTERVAL_ANNUAL = 'Annual';

    private $authorizeNow;
    private $recurrentPaymentId;
    private $nextRecurrency;
    private $startDate;
    private $endDate;
    private $interval;
    private $amount;
    private $country;
    private $createDate;
    private $currency;
    private $currentRecurrencyTry;
    private $provider;
    private $recurrencyDay;
    private $successfulRecurrences;
    private $links;
    private $recurrentTransactions;
    private $reasonCode;
    private $reasonMessage;
    private $status;

    public function __construct($authorizeNow = true)
    {
        $this->setAuthorizeNow($authorizeNow);
    }

    public function jsonSerialize()
    {
        return array_filter(get_object_vars($this));
    }

    public function populate(\stdClass $data)
    {
        $this->authorizeNow = isset($data->AuthorizeNow)? !!$data->AuthorizeNow: false;
        $this->recurrentPaymentId = isset($data->RecurrentPaymentId)? $data->RecurrentPaymentId: null;
        $this->nextRecurrency = isset($data->NextRecurrency)? $data->NextRecurrency: null;
        $this->startDate = isset($data->StartDate)? $data->StartDate: null;
        $this->endDate = isset($data->EndDate)? $data->EndDate: null;
        $this->interval = isset($data->Interval)? $data->Interval: null;

        $this->amount = isset($data->Amount)? $data->Amount: null;
        $this->country = isset($data->Country)? $data->Country: null;
        $this->createDate = isset($data->CreateDate)? $data->CreateDate: null;
        $this->currency = isset($data->Currency)? $data->Currency: null;
        $this->currentRecurrencyTry = isset($data->CurrentRecurrencyTry)? $data->CurrentRecurrencyTry: null;
        $this->provider = isset($data->Provider)? $data->Provider: null;
        $this->recurrencyDay = isset($data->RecurrencyDay)? $data->RecurrencyDay: null;
        $this->successfulRecurrences = isset($data->SuccessfulRecurrences)? $data->SuccessfulRecurrences: null;

        $this->links = isset($data->Interval)? $data->Interval: [];
        $this->recurrentTransactions = isset($data->Interval)? $data->Interval: [];

        $this->reasonCode = isset($data->ReasonCode)? $data->ReasonCode: null;
        $this->reasonMessage = isset($data->ReasonMessage)? $data->ReasonMessage: null;
        $this->status = isset($data->Status)? $data->Status: null;
    }

    public static function fromJson($json)
    {
        $object = json_decode($json);

        $recurrentPayment = new RecurrentPayment();

        if (isset($object->RecurrentPayment)) {
            $recurrentPayment->populate($object->RecurrentPayment);
        }

        return $recurrentPayment;
    }

    public function getRecurrentPaymentId()
    {
        return $this->recurrentPaymentId;
    }

    public function getReasonCode()
    {
        return $this->reasonCode;
    }

    public function getReasonMessage()
    {
        return $this->reasonMessage;
    }

    public function gerNextRecurrency()
    {
        return $this->nextRecurrency;
    }

    public function getAmount()
    {
        return $this->amount;
    }

    public function getCountry()
    {
        return $this->country;
    }

    public function getCreateDate()
    {
        return $this->createDate;
    }

    public function getCurrency()
    {
        return $this->currency;
    }

    public function getCurrentRecurrencyTry()
    {
        return $this->currentRecurrencyTry;
    }

    public function getProvider()
    {
        return $this->provider;
    }

    public function getRecurrencyDay()
    {
        return $this->recurrencyDay;
    }

    public function getSuccessfulRecurrences()
    {
        return $this->successfulRecurrences;
    }

    public function getAuthorizeNow()
    {
        return $this->authorizeNow;
    }

    public function setAuthorizeNow($authorizeNow)
    {
        $this->authorizeNow = $authorizeNow;
        return $this;
    }

    public function getStartDate()
    {
        return $this->startDate;
    }

    public function setStartDate($startDate)
    {
        $this->startDate = $startDate;
        return $this;
    }

    public function getEndDate()
    {
        return $this->endDate;
    }

    public function setEndDate($endDate)
    {
        $this->endDate = $endDate;
        return $this;
    }

    public function getInterval()
    {
        return $this->interval;
    }

    public function setInterval($interval)
    {
        $this->interval = $interval;
        return $this;
    }
}
