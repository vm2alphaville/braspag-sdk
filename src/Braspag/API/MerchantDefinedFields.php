<?php

namespace Braspag\API;

class MerchantDefinedFields implements \JsonSerializable
{

    private $id;
    private $value;

    public function jsonSerialize()
    {
        return array_filter(get_object_vars($this));
    }
	
	public function populate(\stdClass $data)
    {
        $this->id = isset($data->id)? $data->id: null;
        $this->value = isset($data->value)? $data->value: null;        
    }

    public function getId()
    {
        return $this->id;
    }

    public function getValue()
    {
        return $this->value;
    }

    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    public function setValue($value)
    {
        $this->value = $value;
        return $this;
    }

}
